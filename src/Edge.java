/**
 * Edge class:
 * <br> -Creates new edges which represent a train trip between two cities 
 * <br> -Includes the source city, destination city and their corresponding travel time
 * @author Annie Sun (z5075255)
 */
public class Edge {
	/**
	 * Constructs a Edge object 
	 * @param fromCity   The node (city) that the edge (trip) starts from
	 * @param toCity     The node (city) that the edge (trip) finishes at
	 * @param travelTime The weight of the edge representing the time taken to travel between the two cities
	 */
	public Edge (Node fromCity, Node toCity, int travelTime) {
		this.fromCity = fromCity;
		this.toCity = toCity;
		this.travelTime = travelTime;
	}
		
	/**
	 * Returns the time taken to travel between two cities as an integer
	 * @return Time taken in minutes as an int
	 */
	public int getTravelTime() {
		return this.travelTime;
	}
	
	/**
	 * Returns the node object (city) of where the trip starts at
	 * @return Node object of city where trip starts at
	 */
	public Node getFromCity() {
		return this.fromCity;
	}
	
	/**
	 * Returns the node object (city) of where the trip finished at
	 * @return Node object of city where trip finished at
	 */
	public Node getToCity() {
		return this.toCity;
	}

	private int travelTime;
	private Node toCity;
	private Node fromCity;
}
