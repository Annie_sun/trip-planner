import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * TripPlanner class deals with: 
 * <br>- Reading input from user 
 * <br>- Parsing all relevant information in preparation for an a* search
 * <br>- Contains a list of required trips that we need and a graph
 * 
 * <br><br>
 * <i> Note: <br>
 * -The performance analysis is in AStarSearch class for TripHeuristic and SimpleExpansion<br>
 * -The secondHeuristic analysis is in its own class </i>
 * 
 * @author Annie Sun (z5075255) 
 */

public class TripPlanner {

	/**
	 * Constructs a TripPlanner object
	 * 
	 */
	public TripPlanner() {
		this.map = new TripPlannerGraph();
		this.requiredTrips = new ArrayList<Edge>();
	}
	
	public static void main(String[] args) {

		/**
		 * TripPlanner main
		 * @param input from the file 
		 */
		Scanner input = null;
	    TripPlanner planner = new TripPlanner();	
	
		try {				
			input = new Scanner(new FileReader(args[0]));  

			String cityName;
			String fromCity, toCity;
			int transferTime, travelTime;
			
			while (input.hasNext()) {
				String currLine = input.next();
				
				// Transfer <time> <name>
				if (currLine.equals(COMMAND_TRANSFER)) {
					transferTime = input.nextInt();
					cityName = input.next(); 		
					planner.map.addNode(cityName, transferTime); 		
					
				// Time <time> <name1> <name2>
				} else if (currLine.equals(COMMAND_TIME)) { 	
					travelTime = input.nextInt();
					fromCity = input.next();
					toCity = input.next();
					planner.map.addEdge(fromCity, toCity, travelTime);
					
				// Trip <name1> <name2>
				} else if (currLine.equals(COMMAND_TRIP)) {
					fromCity = input.next();
					toCity = input.next();
					Edge req = planner.map.getEdge(fromCity, toCity);
					planner.requiredTrips.add(req);
					
				} else {
					// invalid input
				}
			}
			
			AStarSearch newSearch = new AStarSearch();			
			newSearch.search(planner.map, planner.requiredTrips);		

		} catch (FileNotFoundException e) {

		} finally {
			if (input != null) {
				input.close();
			}
		}			
	}
	
	private static String COMMAND_TRANSFER = "Transfer";
	private static String COMMAND_TIME = "Time";
	private static String COMMAND_TRIP = "Trip";
	
	private Graph map;
	private ArrayList<Edge> requiredTrips;
}
