import java.util.ArrayList;
/**
 * TripHeuristic class:
 * <br>- Implements a basic heuristic for a*search <br><br>
 * <i> Same but more condensed explanation than explanation in AStarSearch class </i>
 * <p>  This heuristic sums the remaining travel time of the remaining required trips of the current
 * state. A heuristic is admissible when <tt> For all values of n h(n) is less than or equal to
 * h*(n) where h*(n)  is true cost from n to goal.
 * </tt> This heuristic is always admissible because the final solution will have to include 
 * the travel times of the required time. Therefore since it will never overestimate the cost
 * from the current state to the goal state, the heuristic is considered to be admissible.
 * 
 * The performance of this heuristic is O(E) where E stands for number of remaining edges(trips) 
 * in the tripsLeft arrayList. This is because it iterates through E number of 
 * edges when finding the total cost. 
 * @author Annie Sun (z5075255)
 */
public class TripHeuristic implements Heuristic {

	/**
	 * Returns an estimate of cost between the current state and goal state as an integer
	 * @param curr Node object that the current state is on 
	 * @param tripsLeft An arraylist of all the edges that still need to be traversed
	 * @return integer value representing the estimated cost between current and goal state
	 * @Override
	 */
	public int heuristicValue (Node curr, ArrayList<Edge> tripsLeft) {
		int totalCost = 0;
	
		for (Edge e: tripsLeft) {
			totalCost += e.getTravelTime();
		}
		return totalCost;
	}
	
}
