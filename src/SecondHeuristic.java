import java.util.ArrayList;
/**
 * TripHeuristic class:
 * <br> - Yet another possible heuristic
 * <br> - More advanced version of tripHeuristic
 * <br> - As well as calculating total travel time, transfer time between train trips
 * are also considered. But to make it admissible, the highest transfer time is deducted 
 * from the total cost. This is because if the last transfer time is smaller than the rest, 
 * the heuristic will end up overestimating. By removing the largest transfer time, the heuristic 
 * will always either equal or underestimate total cost to the goal. 
 * <br> - However after some testing, it seems that the tripHeuristic perform
 * better overall. But there are some cases where this heuristic expands a significantly 
 * lower amount of nodes which decreases running time. 
 * Considering the purpose of the program, it is more optimal to use a heuristic 
 * that expands less nodes more frequently and consistently
 * <br> - Similarly to tripHeuristic, the performance of this heuristic is O(E) where E 
 * stands for number of remaining edges(trips) in the tripsLeft arrayList.
 * @author Annie Sun (z5075255)
 *
 */
public class SecondHeuristic implements Heuristic {

	/**
	 * Returns an estimate of cost between the current state and goal state as an integer
	 * @param curr Node object that the current state is on 
	 * @param tripsLeft An arraylist of all the edges that still need to be traversed
	 * @return integer value representing the estimated cost between current and goal state
	 * @Override
	 */
	public int heuristicValue (Node curr, ArrayList<Edge> tripsLeft) {
		int totalCost = 0;
		int largest = 0;
		int transferTime = 0;
		
		for (Edge e : tripsLeft) {
			transferTime = e.getToCity().getTransferTime();		
			
			if (largest < transferTime) {
				largest = transferTime;
			}	
			totalCost += e.getTravelTime();		
		}		
		totalCost = totalCost - largest;
	
		return totalCost;
	}
}
