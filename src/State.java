import java.util.ArrayList;
/**
 * State class:
 * <br> -Stores the state of the current node of the search 
 * <br> -Includes an arraylist of remaining edges needed to traversed
 * <br> -In charge of determining whether a state is identical to a pre-existing state
 * <br> -Sorts the state objects in order of increasing fn cost 
 * @author Annie Sun (z5075255)
 *
 */
public class State implements Comparable<State> {

	/**
	 * Constructs a State object 
	 * @param tripsLeft An arraylist of all the edges that still need to be traversed
	 * @param prevState The parent of this state.
	 * @param gn        The cost of all previous states + this one 
	 * @param hn        The heuristic value that estimates approximate time to the goal state
	 * @param curr      Node object of the curr state 
	 */
	public State(ArrayList<Edge> tripsLeft, State prevState, int gn, int hn, Node curr){
		this.tripsLeft = tripsLeft;
		this.prevState = prevState;
		this.fn = gn + hn;
		this.gn = gn;
		this.node = curr;	
	}
	
	/**
	 * Returns an arraylist of edges that still need to be traversed
	 * @return Arraylist of remaining edges/trips
	 */
	public ArrayList<Edge> getTripsLeft() {
		return this.tripsLeft;
	}
	
	/**
	 * Returns the parent of the state as a State object
	 * @return Parent/previous state of this state
	 */
	public State getPrevState() {
		return this.prevState;
	}
	
	/**
	 * Returns the total cost of this state as an integer. 
	 * <br>This is the sum of gn + hn of this state + all previous states
	 * @return Fn cost of this state
	 */
	public int getFn(){
		return this.fn;
	}
	/**
	 * Returns the total cost of this state as an integer
	 * <br> Sum of all gn of this state + all previous
	 * @return Gn cost of this state
	 */
	public int getGn(){
		return this.gn;
	}
	
	/**
	 * Returns the node of the current state
	 * @return Node of current state
	 */
	public Node getNode() {
		return this.node;
	}
	
	/**
	 * Comparator method used to sort states in order of their total cost (fn) in the priority queue
	 * @Override
	 */
	public int compareTo(State o) {
		if (this.fn > o.fn) {
			return 1;
		} 
		if (this.fn == o.fn) {
			return 0;
		}
		if (this.fn < o.fn) {
			return -1;
		}
		return 0;
	}
	
	/**
	 * Equals method used to determine whether a state is identical to an existing one
	 * @Override
	 */
	
	public boolean equals(Object o) {
		if (o == null)
			return false;
		
		State s = (State) o;
		if (this.tripsLeft.equals(s.getTripsLeft())
				&& this.node.equals(s.getNode())) {
			return true;
		}
		return false;
	}
		
	private ArrayList<Edge> tripsLeft;
	private State prevState;
	private int fn;
	private int gn;
	private Node node;
}
