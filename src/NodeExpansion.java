import java.util.ArrayList;

/**
 * Interface used to encapsulate different methods of expansion (only one in this file)
 * @author Annie Sun (z5075255) 
 */
public interface NodeExpansion {
	
	/**
	 * Chooses what states to expand and then returns them as an arraylist  
	 * @param s   State object of the current state
	 * @param h   Heuristic object containing the heuristic value for the current state
	 * @return    Arraylist of all the states that need to be added to the priority queue
	 */
	public ArrayList<State> expandStates (State s, Heuristic h);
}
