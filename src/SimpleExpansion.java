import java.util.ArrayList;

/**
 * SimpleExpansion class:
 * <br> - Implements NodeExpansion
 * <br> - Selects which states to be added on the priority queue 
 * <br> - Returns the list of states to AStarSearch class 
 * @author Annie Sun (z5075255)
 *
 */

public class SimpleExpansion implements NodeExpansion {
	
	/**
	 * Chooses what states to expand and then returns them as an arraylist  
	 * @param s   State object of the current state
	 * @param h   Heuristic object containing the heuristic value for the current state
	 * @return    Arraylist of all the states that need to be added to the priority queue
	 * @Override
	 */
	public ArrayList<State> expandStates(State s, Heuristic h) {
		ArrayList<State> nextStates = new ArrayList<State>();
		State n = null;
		int gn, hn;
		Node to, from;
		ArrayList<Edge> trips = s.getTripsLeft();
	
		for (Edge e: trips) {			
			ArrayList<Edge> currTripsLeft = new ArrayList<Edge>(trips);
			to = e.getToCity();
			from = e.getFromCity();
		
			// if it is a required trip then remove the edge of requiredTrips
			if (e.getFromCity().equals(s.getNode())) {		
				currTripsLeft.remove(e);
				
				if(!currTripsLeft.isEmpty()) {
					gn = s.getGn() + e.getTravelTime() + to.getTransferTime();
				} else {
					// this is last trip. no need for transfer time
					gn = s.getGn() + e.getTravelTime();
				}
				hn = h.heuristicValue(from, currTripsLeft);
				n = new State(currTripsLeft, s, gn, hn, to);
				nextStates.add(n);
			// or else nothing happens to requiredTrips	
			} else {
				gn = s.getGn() + s.getNode().distanceTo(from) + from.getTransferTime();				
				hn = h.heuristicValue(from, currTripsLeft);
				n = new State(currTripsLeft, s, gn, hn, from);
				nextStates.add(n);	
			}
		}		 		
		return nextStates;
	}
}
