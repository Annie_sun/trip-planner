import java.util.ArrayList;
import java.util.PriorityQueue;
/**
 * AStarSearch class:
 * <br> -Uses a priority queue to sort fn 
 * <br> -Receives a list of states to add onto the priority queue from the NodeExpansion interface
 * <br> -Traverses through the graph to find the optimal path
 * <br> -When goal state is received, it prints out the optimal path, cost and nodes expanded 
 * <br>
 * 
 * <p> This a* search can run in two different modes:
 * <br> 1. Zero Heuristic and SimpleExpansion (slower ver)
 * <br> or
 * <br> 2. TripHeuristic and SimpleExpansion (much quicker ver)
 * <br> <br>
 * The running time of the search is improved in two different ways. <br>
 * <br> 1. Careful selection of what states to be inserted/expanded in the priority queue <br>
 * <br> In State class, we check for identical states by comparing the required trips
 * left and the state's curr node. As well, in this class, if the state is already in the list of visited 
 * states, it is not added into the priority queue. By doing this, we can avoid having to expand multiple
 * states that are the same on the priority queue. Thus, we can reduce the number of expansions 
 * done by the search. As well as optimizing time, we can save a lot of memory as well since the less nodes 
 * expanded results in lower memory usage. <br>
 * 
 * <br> 2. By implementing a heuristic, the edges that are more likely to be part of the optimal trip 
 * are given higher priority in the priority queue. 
 * 
 *  <p>The heuristic sums the remaining travel time of the remaining required trips of the current
 * state. A heuristic is admissible when: <tt> For all values of n h(n) is less than or equal to
 * h*(n) where h*(n)  is true cost from n to goal.
 * </tt> This heuristic is always admissible because the final solution will have to include 
 * the travel times of the required time. Therefore since it will never overestimate the cost
 * from the current state to the goal state, the heuristic is considered to be admissible.
 * The performance of this heuristic is O(E) where E stands for number of remaining edges(trips) 
 * in the tripsLeft arrayList. This is because it iterates through E number of 
 * edges when finding the total cost. So as the number of required trips decrease, the faster the
 * heuristic is at computing the total heuristic cost. However since it is linear and the number
 * of required trips are usually very small, this delay is considered to be negligible compared to 
 * overall time it takes to run the program.
 * 
 * @author Annie Sun (z5075255)
 */

public class AStarSearch {
	
	/**
	 * Searches through the graph for an optimal trip
	 * @param map           Graph object with a list of all nodes and edges 
	 * @param startLocation Node of the location where the search starts from (in this case, it is always London)
	 * @param requiredTrips Arraylist of all the edges that still need to be traversed 
	 */
	public void search (Graph map, ArrayList<Edge> requiredTrips) {
		
		Heuristic heuristic = new TripHeuristic();
		NodeExpansion states = new SimpleExpansion();
		PriorityQueue<State> searchQueue = new PriorityQueue<State>();
		ArrayList<State> visited = new ArrayList<State>();
				
		int numNodes = 0;
		int hn;
		State curr = null;
		State london = null;
	
		Node startLocation = map.getNode("London");
		hn = heuristic.heuristicValue(startLocation, requiredTrips);			
		london = new State(requiredTrips, null, 0, hn, startLocation);
		searchQueue.add(london);
		
		while (!searchQueue.isEmpty()) {
			curr = searchQueue.remove();
		
			if (visited.contains(curr)) {		
				continue;		
			}		
			visited.add(curr);
			numNodes ++;	
			
			if (finishedTrip(curr)) {
				break;
			}
			
			ArrayList<State> nextStates = states.expandStates(curr, heuristic);		
			for (State s: nextStates) {	
				if (!visited.contains(s)) {				
					searchQueue.add(s);
				} 
			}			
		}
		
		// prints number of nodes expanded, cost and optimal sequence of trips
		System.out.println(numNodes + " nodes expanded");
		System.out.println("cost = " + curr.getGn());
		ArrayList<Edge> optimalTrip = new ArrayList<Edge>();
		Node from, to;
		String fromName, toName;	
		while (curr!= null) {
			if (curr.getPrevState() != null) {		
				from = curr.getPrevState().getNode();
				to = curr.getNode();
				optimalTrip.add(0 , map.getNodeEdge(from, to));			
			}
			curr = curr.getPrevState();
		}		
		for (Edge e: optimalTrip) {
			fromName = e.getFromCity().getCityName();
			toName = e.getToCity().getCityName();
			System.out.println("Trip " + fromName + " to " + toName);   
		}	
	}
	
	/**
	 * Private method that returns whether all required trips have been traveled
	 * @param curr Node object of current node
	 * @return     Boolean value of whether the trip is finished or not 
	 */
	private boolean finishedTrip (State curr) {
		boolean finished = false;
	
		if (curr.getTripsLeft().isEmpty()) {
			finished = true;
			return finished;
		}		
		return finished;
	}
	
}
