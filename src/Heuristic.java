import java.util.ArrayList;
/**
 * Interface used to encapsulate different heuristics 
 * @author Annie Sun (z5075255)
 */

public interface Heuristic {
		/**
		 * Returns an estimate of cost between the current state and goal state as an integer
		 * @param curr Node object that the current state is on 
		 * @param tripsLeft An arraylist of all the edges that still need to be traversed
		 * @return integer value representing the estimated cost between current and goal state
		 */
		public int heuristicValue (Node curr, ArrayList<Edge> tripsLeft);
}
