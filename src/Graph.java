
/**
 * Interface 
 * @author Annie Sun (z5075255)
 *
 */
public interface Graph {

	/**
	 * Adds a node object representing the city 
	 * @param city The name of the city as a string
	 * @param time The delay of waiting for next trip as an int
	 */
	public void addNode (String city, int time);
	
	/**
	 * Adds an edge object representing the trip between two cities
	 * @param city  The name of city where edge starts from
	 * @param city2 The name of city where edge ends at
	 * @param time  The time to travel between the two cities
	 */
	public void addEdge (String city, String city2, int time);
	
	/**
	 * Returns a Node object for a specified string
	 * @param city Name of the city as a string
	 * @return     Node object for city 
	 */
	public Node getNode (String city);
	
	/**
	 * Returns an Edge object for the trip between two cities
	 * @param city  The name of city where edge starts from
	 * @param city2 The name of city where edge finishes at
	 * @return      The edge between two cities as an Edge Object 
	 */
	public Edge getEdge (String city, String city2);
	
	/**
	 * Returns an Edge object for the trip between two nodes
	 * @param city   The node where edge starts from
	 * @param city2  The node where edge finishes at
	 * @return       The edge between two node as an Edge Object
	 */
	public Edge getNodeEdge(Node city, Node city2);


}
