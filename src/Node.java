import java.util.HashMap;

/** 
 * Node class:
 * <br> - Creates new nodes which represent a city
 * <br> - Includes the city name, time taken to wait for the next train  and a list of cities where you can go
 * @author Annie Sun (z5075255)
 */
public class Node {

	/**
	 * Constructs a Node Object
	 * @param cityName      Name of the city 
	 * @param transferTime  Time taken to wait for the next train
	 */
	public Node(String cityName, int transferTime) {
		this.cityName = cityName; 
		this.transferTime = transferTime;
		this.trainDestList = new HashMap<Node, Integer>(); 
	}
	/**
	 * Adds a train trip and adds it to the list of all train trips
	 * @param dest        Other city which this city is connected to 
	 * @param travelTime  Time taken to travel between this node and destination
	 */
	public void addTrainTrip(Node dest, Integer travelTime) {
		trainDestList.put(dest, travelTime);	
	}
	
	/**
	 * Returns the name of the city as a string 
	 * @return Name of city 
	 */
	public String getCityName() {
		return this.cityName;
	}
	
	/**
	 * Returns the time taken to wait for the next train as an integer
	 * @return Time taken to wait for the next train 
	 */
	public int getTransferTime() {
		return this.transferTime;
	}
	
	/**
	 * Returns an integer value representing the time it takes to travel to another city
	 * @param dest Node object of the city where the node wants to traverse to
	 * @return time taken to travel to destination
	 */
	public Integer distanceTo(Node dest) {	
	    return trainDestList.get(dest);	
	}
	
	private String cityName;
	private int transferTime;
	private HashMap<Node, Integer> trainDestList;
}
