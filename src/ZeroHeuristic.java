import java.util.ArrayList;
/**
 * ZeroHeuristic class:
 * <br> -Zero heuristic value making a*search into Dijkstra search
 * @author Annie Sun (z5075255)
 */
public class ZeroHeuristic implements Heuristic {

	/**
	 * Returns 0 as the heuristic value (essentially fn = gn)
	 * @param curr Node object that the current state is on  (not used)
	 * @param tripsLeft An arraylist of all the edges that still need to be traversed (not used)
	 * @return integer value representing the estimated cost between current and goal state
	 * @Override
	 */
	public int heuristicValue(Node curr, ArrayList<Edge> tripsLeft) {		
		return 0;
	}
}
