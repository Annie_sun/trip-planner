import java.util.ArrayList;
/**
 * TripPlannerGraph class:
 * <br> - Implements Graph interface
 * <br> - Directed graph 
 * <br> - Contains the nodes (cityList) and edges (trainTripList) of the graph 
 * <br> - Can add new nodes (representing cities) and new edges (representing train trips)
 * @author Annie Sun (z5075255)
 *
 */
public class TripPlannerGraph implements Graph {
	/**
	 * Constructs a new TripPlannerGraph 
	 * <br> Stores a list of all cities and train trips
	 */
	public TripPlannerGraph() {
		this.cityList = new ArrayList <Node>();
		this.trainTripList = new ArrayList<Edge>();
	}

	/**
	 * Creates a Node Object for the specified city and adds it to the list of all cities
	 * @param city         Name of city as a string
	 * @param transferTime Time taken to transfer to another train (delay) as an int
	 * @Override
	 */
	public void addNode(String city, int transferTime) {
		Node newCity = new Node (city, transferTime);
		cityList.add(newCity);		
	}

	/**
	 * Creates an Edge Object for the train trip and adds it to the list of all train trips
	 * @param fromCity   The node (city) that the edge (trip) starts from
	 * @param toCity     The node (city) that the edge (trip) finishes at
	 * @param travelTime The weight of the edge representing the time taken to travel between the two cities
	 * @Override
	 */	
	public void addEdge(String fromCity, String toCity, int travelTime) {

		Node to = getNode(toCity);
		Node from = getNode(fromCity);

		// directed graph so have to add both to -> from and from -> to	
		to.addTrainTrip(from, travelTime);
		from.addTrainTrip(to, travelTime);

		Edge forwardTrip = new Edge (from, to, travelTime);
		trainTripList.add(forwardTrip);
		Edge backwardTrip = new Edge (to, from, travelTime);
		trainTripList.add(backwardTrip);
	}

	/**
	 * Returns the Node object for a specified string/cityName
	 * @param cityName Name of the city as a string
	 * @return         The node of a specified city as a Node Object
	 * @Override
	 */
	public Node getNode(String cityName) {
		Node city = null;

		for (Node n: cityList) {
			String currCity = n.getCityName();
			if (currCity.equals(cityName)) {
				city = n;	
				break;
			}		
		}
		return city;
	}

	/**
	 * Returns the Edge Object for a specified trip between two cities 
	 * @param fromCity Source city name as a string
	 * @param toCity   Destination city name as a string 
	 * @return         The edge between two cities as an Edge Object 
	 * @Override
	 */
	public Edge getEdge(String fromCity, String toCity) {
		Edge trainTrip = null;
		Node currFrom, currTo;

		for (Edge e: trainTripList) {	
			currFrom = e.getFromCity();
			currTo = e.getToCity();

			if ((currTo.getCityName().equals(toCity)) && currFrom.getCityName().equals(fromCity)) {
				trainTrip = e;
				break;
			}
		}
		return trainTrip;		
	}
	
	/**
	 * Returns the Edge Object for the trip between two nodes
	 * @param fromCity The node (city) that the edge (trip) starts from
	 * @param toCity   The node (city) that the edge (trip) finished at
	 * @return         The edge between two nodes as an Edge Object 
	 * @Override
	 */
	public Edge getNodeEdge (Node fromCity, Node toCity) {
		Edge trainTrip = null;
		Node currFrom, currTo;
		for (Edge e: trainTripList) {
			currFrom = e.getFromCity();
			currTo = e.getToCity();

			if ((currTo.equals(toCity)) && currFrom.equals(fromCity)) {
				trainTrip = e;
				break;
			}
		}
		return trainTrip;
	}
		
	private ArrayList<Node> cityList; 
	private ArrayList<Edge> trainTripList;
}
